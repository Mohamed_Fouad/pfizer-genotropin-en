﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PfizerAnalyicManager : MonoBehaviour
{


    #region Singleton
    private static PfizerAnalyicManager _instance;
    public static PfizerAnalyicManager Instance { get { return _instance; } }
    #endregion

#if !UNITY_EDITOR && UNITY_WEBGL
    [System.Runtime.InteropServices.DllImport("__Internal")]
    static extern void fireAnalysis(string str);
#endif


    void Awake()
    {
        #region Singleton
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
        #endregion
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void FirePfAnalysis(string str)
    {
#if !UNITY_EDITOR && UNITY_WEBGL
            fireAnalysis(str);
#endif
    }
}
