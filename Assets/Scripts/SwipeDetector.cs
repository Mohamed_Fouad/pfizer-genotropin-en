﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwipeDetector : MonoBehaviour
{
    Touch touch;
    Vector2 beginTouchPosition, endTouchPosition;
    int currentIndex = 0;
   // List<>
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.touchCount > 0)
        {
            touch = Input.GetTouch(0);
            switch (touch.phase)
            {
                case TouchPhase.Began:
                    beginTouchPosition = touch.position;
                    break;
                case TouchPhase.Ended:
                    endTouchPosition = touch.position;
                    if (beginTouchPosition.x > endTouchPosition.x)
                    {
                        Debug.Log("Left Swipe");
                    }
                    if (beginTouchPosition.x < endTouchPosition.x)
                    {
                        Debug.Log("Right Swipe");
                    }
                    break;
            }
        }
    }
}
