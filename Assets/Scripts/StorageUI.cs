﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StorageUI : MonoBehaviour
{
    #region Singleton
    private static StorageUI _instance;
    public static StorageUI Instance { get { return _instance; } }
    #endregion

    Touch touch;
    Vector2 beginTouchPosition, endTouchPosition;
    int currentIndex = 0;
    int lastVisitedIndex = 0;

    [Header("UI Assignment")]
    [SerializeField] GameObject storageUIPlaceHolder;
    [SerializeField] Button storageUICloseBtn;
    [SerializeField] List<GameObject> storageSlides;

    #region Unity Methods
    void Awake()
    {
        #region Singleton
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
        #endregion
    }

    void Start()
    {
        storageUICloseBtn.onClick.AddListener(StorageCloseBtnClk);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.touchCount > 0 && storageUIPlaceHolder.activeInHierarchy)
        {
            touch = Input.GetTouch(0);
            switch (touch.phase)
            {
                case TouchPhase.Began:
                    beginTouchPosition = touch.position;
                    break;
                case TouchPhase.Ended:
                    endTouchPosition = touch.position;
                    if (beginTouchPosition.x > endTouchPosition.x)
                    {
                        if (currentIndex != (storageSlides.Count - 1))
                        {
                            lastVisitedIndex = currentIndex;
                            currentIndex++;
                            ShowCurrentSlide();
                        }
                    }
                    if (beginTouchPosition.x < endTouchPosition.x)
                    {
                        if (currentIndex != 0)
                        {
                            lastVisitedIndex = currentIndex;
                            currentIndex--;
                            ShowCurrentSlide();
                        }
                    }
                    break;
            }
        }
    }
    #endregion

    #region Public Methods
    public void ShowStorageUI()
    {
        storageUIPlaceHolder.SetActive(true);
        currentIndex = 0;
        lastVisitedIndex = 0;
        storageSlides[currentIndex].SetActive(true);
    }

    public void HideStorageUI()
    {
        storageUIPlaceHolder.SetActive(false);
    }
    #endregion

    #region Private Methods
    void ShowCurrentSlide()
    {
        if (lastVisitedIndex != currentIndex)
        {
            storageSlides[lastVisitedIndex].SetActive(false);
        }
        storageSlides[currentIndex].SetActive(true);
    }

    void StorageCloseBtnClk()
    {
        storageSlides[currentIndex].SetActive(false);
        HideStorageUI();
    }
    #endregion
}
