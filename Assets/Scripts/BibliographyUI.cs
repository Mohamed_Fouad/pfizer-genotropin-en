﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BibliographyUI : MonoBehaviour
{
    #region Singleton
    private static BibliographyUI _instance;
    public static BibliographyUI Instance { get { return _instance; } }
    #endregion

    [SerializeField] GameObject bibliographyUIIPlaceHolder;
    [SerializeField] Button bibliographyCloseBtn;

    #region Unity Methods
    void Awake()
    {
        #region Singleton
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
        #endregion
    }

    void Start()
    {
        bibliographyCloseBtn.onClick.AddListener(CloseBtnClk);
    }

    void Update()
    {

    }
    #endregion

    #region Private Methods
    void CloseBtnClk()
    {
        HideBibliographyUI();
    }
    #endregion

    #region Public Methods
    public void ShowBibliographyUI()
    {
        bibliographyUIIPlaceHolder.SetActive(true);
    }

    public void HideBibliographyUI()
    {
        bibliographyUIIPlaceHolder.SetActive(false);
    }
    #endregion
}
