﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LandingPageUI : MonoBehaviour
{
    #region Singleton
    private static LandingPageUI _instance;
    public static LandingPageUI Instance { get { return _instance; } }
    #endregion

    [SerializeField] GameObject LandingPageUIPlaceHolder;
    [SerializeField] GameObject TCPanel;
    [SerializeField] GameObject LangPage;
    [SerializeField] Button aboutAppCloseBtn;
    [SerializeField] Link linkScrpt;




    #region Unity Methods
    void Awake()
    {
        #region Singleton
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
        #endregion
    }

    void Start()
    {
        aboutAppCloseBtn.onClick.AddListener(CloseBtnClk);


        if (!PlayerPrefs.GetString("firstLanding").Equals("no"))
        {
            ShowLandingPageUI();

        }
        else
            HideLandingPageUI();

    }

  
    #endregion

    #region Private Methods
    void CloseBtnClk()
    {
        HideLandingPageUI();
    }
    #endregion

    #region Public Methods
    public void ShowLandingPageUI()
    {
        if (!LangPage.activeInHierarchy)
        {
            LandingPageUIPlaceHolder.SetActive(true);
            StartCoroutine("WaitAndStart");
        }
        else
            ShowLandingPageUI();
    }

    private IEnumerator WaitAndStart()
    {
        yield return new WaitForSeconds(5.0f);
        TCPanel.SetActive(true);
        // Open js
        linkScrpt.OpenLinkJSPlugin("terms");
    }


    public void HideLandingPageUI()
    {
        LandingPageUIPlaceHolder.SetActive(false);
        PlayerPrefs.SetString("firstLanding","no");
        PlayerPrefs.Save();
    }
    #endregion
}
