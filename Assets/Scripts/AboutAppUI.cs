﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AboutAppUI : MonoBehaviour
{
    #region Singleton
    private static AboutAppUI _instance;
    public static AboutAppUI Instance { get { return _instance; } }
    #endregion

    [SerializeField] GameObject aboutAppUIPlaceHolder;
    [SerializeField] Button aboutAppCloseBtn;

    #region Unity Methods
    void Awake()
    {
        #region Singleton
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
        #endregion
    }

    void Start()
    {
        aboutAppCloseBtn.onClick.AddListener(CloseBtnClk);
    }

    void Update()
    {
        
    }
    #endregion

    #region Private Methods
    void CloseBtnClk()
    {
        HideAboutAppUI();
    }
    #endregion

    #region Public Methods
    public void ShowAboutAppUI()
    {
        aboutAppUIPlaceHolder.SetActive(true);
    }

    public void HideAboutAppUI()
    {
        aboutAppUIPlaceHolder.SetActive(false);
    }
    #endregion
}
