﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenuUIManager : MonoBehaviour
{
    #region Singleton
    private static MainMenuUIManager _instance;
    public static MainMenuUIManager Instance { get { return _instance; } }
    #endregion
    [SerializeField] GameObject mainMenuUIPlaceHolder;
    [SerializeField] GameObject PWAiOSHolder;

    [Header("UI Assignment")]
    [SerializeField] Button aboutBtn;
    [SerializeField] Button settingsBtn;
    [SerializeField] Button helpBtn;
    [SerializeField] Button injectionAreasBtn;
    [SerializeField] Button storageBtn;
    [SerializeField] Button bibliographyBtn;
    [SerializeField] Button instructionsBtn;
    [SerializeField] Button instructionsDesktopBtn;

    [Header("Footer")]
    [SerializeField] GameObject footerUI;

    bool isMobile = false;

#if !UNITY_EDITOR && UNITY_WEBGL
    [System.Runtime.InteropServices.DllImport("__Internal")]
    static extern bool IsMobile();
#endif

    #if !UNITY_EDITOR && UNITY_WEBGL
         [System.Runtime.InteropServices.DllImport("__Internal")] private static extern string GetBrowserVersion();
    #endif

    void CheckIfMobile()
    {
#if !UNITY_EDITOR && UNITY_WEBGL
        isMobile = IsMobile();
        instructionsBtn.gameObject.SetActive(isMobile);//ismobile
        instructionsDesktopBtn.gameObject.SetActive(!isMobile);//!ismobile
#endif
    }

    public void  CheckBrowser()
    {
#if !UNITY_EDITOR && UNITY_WEBGL
            string browserVersion = GetBrowserVersion ();
  ShowPWAiOS(browserVersion);
#endif
    }

    void ShowPWAiOS(string browserVersionStr)
    {
        
        if ((browserVersionStr.Contains("safari") || browserVersionStr.Contains("Safari") ) && !browserVersionStr.Contains("PWA"))
        PWAiOSHolder.SetActive(true);
    }
    #region Unity Methods
    void Awake()
    {
        #region Singleton
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
        #endregion
    }
    void Start()
    {
        aboutBtn.onClick.AddListener(AboutBtnClk);
        settingsBtn.onClick.AddListener(SettingsBtnClk);
        helpBtn.onClick.AddListener(HelpBtnClk);
        injectionAreasBtn.onClick.AddListener(InjectionAreasBtnClk);
        storageBtn.onClick.AddListener(StorageBtnClk);
        bibliographyBtn.onClick.AddListener(BibliographyBtnClk);
        instructionsBtn.onClick.AddListener(instructionsBtnClk);
        
        CheckIfMobile();
       //  CheckBrowser();

    }

    void Update()
    {

    }
    #endregion

    #region Public Methods
    public void ShowMainMenuUI()
    {
        mainMenuUIPlaceHolder.SetActive(true);
        footerUI.SetActive(true);
    }

    public void HideMainMenuUI()
    {
        mainMenuUIPlaceHolder.SetActive(false);
        footerUI.SetActive(false);
    }

   public void OpenURL(string url)
    {

    }
    #endregion

    #region Private Methods
    public void AboutBtnClk()
    {
        AboutAppUI.Instance.ShowAboutAppUI();
    }
    public void SettingsBtnClk()
    {
        SettingsUI.Instance.ShowSettingsUI();
    }
    public void HelpBtnClk()
    {
        HelpUI.Instance.ShowHelpUI();
    }
    public void InjectionAreasBtnClk()
    {
       
    }
    public void StorageBtnClk()
    {
        StorageUI.Instance.ShowStorageUI();
    }
    public void BibliographyBtnClk()
    {
        BibliographyUI.Instance.ShowBibliographyUI();
    }
    public void instructionsBtnClk()
    {
        HideMainMenuUI();
        //SceneManager.LoadScene("AR Scene");
    }
    #endregion
}
