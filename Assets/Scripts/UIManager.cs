﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour
{


    #region Singleton
    private static UIManager _instance;
    public static UIManager Instance { get { return _instance; } }
    #endregion

 

    #region Unity Methods
    void Awake()
    {
        #region Singleton
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
        #endregion
    }
    void Start()
    {

    }

    void Update()
    {

    }
    #endregion
}
