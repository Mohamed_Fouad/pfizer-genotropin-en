using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class VirtualNurseManager : MonoBehaviour
{
    // Start is called before the first frame update
    public Animator NurseAnim;
    private int _currentStepNum = 0;
    public AudioClip[] StepSound;
    public AudioClip[] StepDoseTwoSound;
    public GameObject[] HandsObj;
    public GameObject[] NursesObj;
    public AudioSource AS;
    public Image CircleStepsProgress;
    public Image LineStepsProgress;
    AnimatorClipInfo[] m_CurrentClipInfo;
    public Text StepsCountTxt, StepsTitleTxt;
    public GameObject StepsList, ARMenu;
    public GameObject[] ControlPanelBtns;
    public string[] StepTitle;
    public Image[] StepImg;
    public Color MainColor, SelectedColor;
    public GameObject[] StepsInfoPanels;

    public Image ARmenuImg, StepsListImg;
    public Sprite ARmenuOffSpr, ARmenuOnImgSpr, StepsListOffSpr, StepsListOnSpr;

    public Material PenMat;
    public Material PenMatTransparent;
    public Texture PenTex1;
    public Texture PenTex2;

    bool fillMe;
    bool isStepCompleted;
    public bool doseOne = false;

    private void Start()
    {
        CheckUnIndexedSteps();
        CheckControlPanelBtns();
    }

    public void ResetNurse()
    {
        CheckNurseIdleState();
        CheckAllHandsNotActive();
        CheckStepsPanelNotActive();
        AS.Stop();
        CircleStepsProgress.fillAmount = 0;
        CircleStepsProgress.gameObject.SetActive(false);
        StepsListImg.sprite = StepsListOffSpr;
    }

    public void ChooseTheDose(bool dose)
    {
        doseOne = dose;
        if (dose)
        {
            PenMat.mainTexture = PenTex2;
            PenMatTransparent.mainTexture = PenTex2;
            PenMat.SetTexture("_EmissionMap", PenTex2);
        }
        else
        {
            PenMat.mainTexture = PenTex1;
            PenMatTransparent.mainTexture = PenTex1;
            PenMat.SetTexture("_EmissionMap", PenTex1);
        }
    }

    [SerializeField] public bool CheckIndexed = true;
    public float numberOfSteps = 8;

    public void NextStep(int _stepNum)
    {
        _currentStepNum = _stepNum;

        CheckNurseIdleState();
        CheckAllHandsNotActive();
        CheckStepsPanelNotActive();
        CheckStepListColor();

        StepsTitleTxt.text = StepTitle[_currentStepNum];
        StepImg[_currentStepNum].color = SelectedColor;
        if (doseOne)
        {
            DoseOneSteps(_stepNum);
        }
        else
        {
            DoseTwoSteps(_stepNum);
        }

        if (CheckIndexed)
        {
            if (CheckUnIndexedSteps())
            {
                fillMe = true;
                CircleStepsProgress.fillAmount = (float) _currentStepNum / 8f;
                StepsCountTxt.text = _currentStepNum + "/8";
            }
        }
        else
        {
            CircleStepsProgress.gameObject.SetActive(true);
            fillMe = true;
            CircleStepsProgress.fillAmount = (float) (_currentStepNum + 1) / numberOfSteps;
            StepsCountTxt.text = _currentStepNum + 1 + "/" + numberOfSteps;
        }


        CheckControlPanelBtns();

        // StepsTitleTxt.text = StepTitle[_currentStepNum];

        PfizerAnalyicManager.Instance.FirePfAnalysis("AR_step" + _currentStepNum + "_" + StepTitle[_currentStepNum] +
                                                     "_start");
        isStepCompleted = false;
    }

    private void DoseOneSteps(int _stepNum)
    {
        if (_stepNum == 0)
        {
            NurseAnim.SetTrigger("step0");
            AS.clip = StepSound[_stepNum];
            AS.Play();
        }
        else if (_stepNum == 1)
        {
            NurseAnim.SetTrigger("step1");
            AS.clip = StepSound[_stepNum];
            AS.Play();
            HandsObj[_stepNum - 1].SetActive(true);
        }
        else if (_stepNum == 2)
        {
            NurseAnim.SetTrigger("step2");
            StepsInfoPanels[0].SetActive(true);
            AS.clip = StepSound[_stepNum];
            AS.Play();
            HandsObj[_stepNum - 1].SetActive(true);
        }
        else if (_stepNum == 3)
        {
            NurseAnim.SetTrigger("step3");
            // StepsInfoPanels[1].SetActive(true);
            AS.clip = StepSound[_stepNum];
            AS.Play();
            HandsObj[_stepNum - 1].SetActive(true);
        }
        else if (_stepNum == 4 && doseOne)
        {
            NurseAnim.SetTrigger("step4");
            AS.clip = StepSound[_stepNum];
            AS.Play();
            HandsObj[_stepNum - 1].SetActive(true);
            StepsInfoPanels[1].SetActive(true);
        }
        else if (_stepNum == 4 && !doseOne)
        {
            NurseAnim.SetTrigger("step5");
            AS.clip = StepSound[_stepNum];
            AS.Play();
            HandsObj[_stepNum].SetActive(true);
            StepsInfoPanels[1].SetActive(true);
        }
        else if (_stepNum == 5 && !doseOne)
        {
            NurseAnim.SetTrigger("step6");
            AS.clip = StepSound[_stepNum];
            AS.Play();
            HandsObj[_stepNum].SetActive(true);
            StepsInfoPanels[2].SetActive(true);
        }
        else if (_stepNum == 5 && doseOne)
        {
            NurseAnim.SetTrigger("step6");
            AS.clip = StepSound[_stepNum];
            AS.Play();
            HandsObj[9].SetActive(true);
            StepsInfoPanels[2].SetActive(true);
        }
        else if (_stepNum == 6 && !doseOne)
        {
            print("ssssssssssssssssss");
            NurseAnim.SetTrigger("step7");
            AS.clip = StepSound[_stepNum];
            AS.Play();
            HandsObj[_stepNum].SetActive(true);
        }
        else if (_stepNum == 6 && doseOne)
        {
            NurseAnim.SetTrigger("step7");
            AS.clip = StepSound[_stepNum];
            AS.Play();
            HandsObj[10].SetActive(true);
        }
        else if (_stepNum == 7 && doseOne)
        {
            NurseAnim.SetTrigger("step8");
            AS.clip = StepSound[_stepNum];
            AS.Play();
            HandsObj[_stepNum].SetActive(true);
            StepsInfoPanels[3].SetActive(true);
        }
        else if (_stepNum == 7 && !doseOne)
        {
            NurseAnim.SetTrigger("step8");
            AS.clip = StepSound[_stepNum];
            AS.Play();
            HandsObj[_stepNum].SetActive(true);
            StepsInfoPanels[3].SetActive(true);
        }
        else if (_stepNum == 8)
        {
            NurseAnim.SetTrigger("step9");
            AS.clip = StepSound[_stepNum];
            AS.Play();
            HandsObj[_stepNum].SetActive(true);
            // StepsInfoPanels[2].SetActive(true);
        }
    }

    private void DoseTwoSteps(int _stepNum)
    {
        if (_stepNum == 0)
        {
            NurseAnim.SetTrigger("step0");
            AS.clip = StepDoseTwoSound[0];
            AS.Play();
        }
        else if (_stepNum == 1)
        {
            NurseAnim.SetTrigger("step1");
            AS.clip = StepDoseTwoSound[_stepNum];
            AS.Play();
            HandsObj[_stepNum - 1].SetActive(true);
        }
        else if (_stepNum == 2)
        {
            NurseAnim.SetTrigger("step2");
            StepsInfoPanels[0].SetActive(true);
            AS.clip = StepDoseTwoSound[_stepNum];
            AS.Play();
            HandsObj[_stepNum - 1].SetActive(true);
        }
        else if (_stepNum == 3)
        {
            NurseAnim.SetTrigger("step3");
            // StepsInfoPanels[1].SetActive(true);
            AS.clip = StepDoseTwoSound[_stepNum];
            AS.Play();
            HandsObj[_stepNum - 1].SetActive(true);
        }
        else if (_stepNum == 4 && doseOne)
        {
            NurseAnim.SetTrigger("step4");
            AS.clip = StepDoseTwoSound[_stepNum];
            AS.Play();
            HandsObj[_stepNum - 1].SetActive(true);
            StepsInfoPanels[1].SetActive(true);
        }
        else if (_stepNum == 4 && !doseOne)
        {
            NurseAnim.SetTrigger("step5");
            AS.clip = StepDoseTwoSound[_stepNum];
            AS.Play();
            HandsObj[_stepNum].SetActive(true);
            StepsInfoPanels[1].SetActive(true);
        }
        else if (_stepNum == 5 && !doseOne)
        {
            NurseAnim.SetTrigger("step6");
            AS.clip = StepDoseTwoSound[_stepNum];
            AS.Play();
            HandsObj[_stepNum].SetActive(true);
            StepsInfoPanels[2].SetActive(true);
        }
        else if (_stepNum == 5 && doseOne)
        {
            NurseAnim.SetTrigger("step6");
            AS.clip = StepDoseTwoSound[_stepNum];
            AS.Play();
            HandsObj[9].SetActive(true);
            StepsInfoPanels[2].SetActive(true);
        }
        else if (_stepNum == 6)
        {
            NurseAnim.SetTrigger("step7");
            AS.clip = StepDoseTwoSound[_stepNum];
            AS.Play();
            HandsObj[_stepNum].SetActive(true);
        }
        else if (_stepNum == 7 && doseOne)
        {
            NurseAnim.SetTrigger("step8");
            AS.clip = StepDoseTwoSound[_stepNum];
            AS.Play();
            HandsObj[_stepNum].SetActive(true);
            StepsInfoPanels[3].SetActive(true);
        }
        else if (_stepNum == 7 && !doseOne)
        {
            NurseAnim.SetTrigger("step8");
            AS.clip = StepDoseTwoSound[_stepNum];
            AS.Play();
            HandsObj[_stepNum].SetActive(true);
            StepsInfoPanels[3].SetActive(true);
        }
        else if (_stepNum == 8)
        {
            NurseAnim.SetTrigger("step9");
            AS.clip = StepDoseTwoSound[_stepNum];
            AS.Play();
            HandsObj[_stepNum].SetActive(true);
            // StepsInfoPanels[2].SetActive(true);
        }
    }

    private void Update()
    {
        // FillStepsCircularProgress();
        FillStepLineProgress();
    }


    void CheckControlPanelBtns()
    {
        if (_currentStepNum == 0)
        {
            ControlPanelBtns[0].SetActive(false);
            ControlPanelBtns[1].SetActive(true);
        }
        else if (_currentStepNum == 8)
            ControlPanelBtns[1].SetActive(false);
        else if (_currentStepNum != 0 && _currentStepNum != 8)
        {
            ControlPanelBtns[0].SetActive(true);
            ControlPanelBtns[1].SetActive(true);
        }
    }

    bool CheckUnIndexedSteps()
    {
        if (_currentStepNum == 0 || _currentStepNum == 10)
        {
            //StepsTxt.gameObject.SetActive(false);
            CircleStepsProgress.gameObject.SetActive(false);
            return false;
        }
        else
        {
            // StepsTxt.gameObject.SetActive(true);
            CircleStepsProgress.gameObject.SetActive(true);
            return true;
        }
    }

    void CheckNurseIdleState()
    {
        m_CurrentClipInfo = NurseAnim.GetCurrentAnimatorClipInfo(0);
        if (!m_CurrentClipInfo[0].clip.name.Equals("mixamo.com"))
            NurseAnim.SetTrigger("idle");
    }

    void FillStepsCircularProgress()
    {
        if (fillMe)
            if (CircleStepsProgress.fillAmount.ToString("0.00").Equals(((float) _currentStepNum / 8f).ToString("0.00")))
                fillMe = false;
            else if (CircleStepsProgress.fillAmount < (float) _currentStepNum / 8f)
                CircleStepsProgress.fillAmount += 0.5f / 2 * Time.deltaTime;
            else if (CircleStepsProgress.fillAmount > (float) _currentStepNum / 8f)
                CircleStepsProgress.fillAmount -= 0.5f / 2 * Time.deltaTime;
    }

    void FillStepLineProgress()
    {
        if (AS.clip != null)
        {
            LineStepsProgress.fillAmount = AS.time / AS.clip.length;

            if (LineStepsProgress.fillAmount > 0.95f && !isStepCompleted)
            {
                isStepCompleted = true;
                PfizerAnalyicManager.Instance.FirePfAnalysis("AR_step" + _currentStepNum + "_" +
                                                             StepTitle[_currentStepNum] + "_completed");
            }
        }
    }

    private void CheckAllHandsNotActive()
    {
        foreach (var hand in HandsObj)
        {
            if (hand != null)
                hand.SetActive(false);
        }
    }

    private void CheckStepListColor()
    {
        foreach (Image step in StepImg)
        {
            if (step != null)
                step.color = MainColor;
        }
    }

    private void CheckStepsPanelNotActive()
    {
        foreach (var stepsPanels in StepsInfoPanels)
        {
            if (stepsPanels != null)
                stepsPanels.SetActive(false);
        }
    }


    public void OpenCloseStepsList()
    {
        StepsList.SetActive(!StepsList.activeInHierarchy);
        if (StepsList.activeInHierarchy)
            StepsListImg.sprite = StepsListOnSpr;
        else
            StepsListImg.sprite = StepsListOffSpr;
    }

    public void OpenCloseARMenu()
    {
        ARMenu.SetActive(!ARMenu.activeInHierarchy);
        if (ARMenu.activeInHierarchy)
            ARmenuImg.sprite = ARmenuOnImgSpr;
        else
            ARmenuImg.sprite = ARmenuOffSpr;
    }

    public void ControlPanel(string ActionType)
    {
        if (ActionType.Equals("replay"))
            NextStep(_currentStepNum);
        else if (ActionType.Equals("next"))
            NextStep(++_currentStepNum);
        else if (ActionType.Equals("prev"))
            NextStep(--_currentStepNum);
    }

    public void LoadHome()
    {
        SceneManager.LoadScene("UIScene");
    }
}