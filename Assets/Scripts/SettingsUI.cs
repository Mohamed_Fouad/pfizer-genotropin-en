﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingsUI : MonoBehaviour
{
    #region Singleton

    private static SettingsUI _instance;

    public static SettingsUI Instance
    {
        get { return _instance; }
    }

    #endregion

    [SerializeField] GameObject settingsUIPlaceHolder;
    [SerializeField] Button settingsCloseBtn;

    #region Unity Methods

    void Awake()
    {
        #region Singleton

        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }

        #endregion
    }

    void Start()
    {
        settingsCloseBtn.onClick.AddListener(CloseBtnClk);
    }

    void Update()
    {
    }

    #endregion

    #region Private Methods

    void CloseBtnClk()
    {
        HideSettingsUI();
    }

    #endregion

    #region Public Methods

    public void ShowSettingsUI()
    {
        settingsUIPlaceHolder.SetActive(true);
    }

    public void SetLanguageURL(string url)
    {
        Application.OpenURL(url);
    }

    public void HideSettingsUI()
    {
        settingsUIPlaceHolder.SetActive(false);
    }

    #endregion
}