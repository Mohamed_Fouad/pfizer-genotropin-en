﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InjectionSitesUI : MonoBehaviour
{
    #region Singleton
    private static InjectionSitesUI _instance;
    public static InjectionSitesUI Instance { get { return _instance; } }
    #endregion

    [SerializeField] GameObject injectionSitesUIIPlaceHolder;
    [SerializeField] Button injectionSitesCloseBtn;

    #region Unity Methods
    void Start()
    {
        
    }

    void Update()
    {
        
    }
    #endregion
}
