﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HelpUI : MonoBehaviour
{
    #region Singleton
    private static HelpUI _instance;
    public static HelpUI Instance { get { return _instance; } }
    #endregion

    Touch touch;
    Vector2 beginTouchPosition, endTouchPosition;
    int currentIndex = 0;
    int lastVisitedIndex = 0;

    [Header("UI Assignment")]
    [SerializeField] GameObject helpUIPlaceHolder;
    [SerializeField] Button helpUICloseBtn;
    [SerializeField] List<GameObject> helpSlides;

    #region Unity Methods
    void Awake()
    {
        #region Singleton
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
        #endregion
    }

    void Start()
    {
        helpUICloseBtn.onClick.AddListener(HelpCloseBtnClk);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.touchCount > 0 && helpUIPlaceHolder.activeInHierarchy)
        {
            touch = Input.GetTouch(0);
            switch (touch.phase)
            {
                case TouchPhase.Began:
                    beginTouchPosition = touch.position;
                    break;
                case TouchPhase.Ended:
                    endTouchPosition = touch.position;
                    if (beginTouchPosition.x > endTouchPosition.x)
                    {
                        if (currentIndex != (helpSlides.Count - 1))
                        {
                            lastVisitedIndex = currentIndex;
                            currentIndex++;
                            ShowCurrentSlide();
                        }
                    }
                    if (beginTouchPosition.x < endTouchPosition.x)
                    {
                        if (currentIndex != 0)
                        {
                            lastVisitedIndex = currentIndex;
                            currentIndex--;
                            ShowCurrentSlide();
                        }
                    }
                    break;
            }
        }
    }
    #endregion

    #region Public Methods
    public void ShowHelpUI()
    {
        helpUIPlaceHolder.SetActive(true);
        currentIndex = 0;
        lastVisitedIndex = 0;
        helpSlides[currentIndex].SetActive(true);
    }

    public void HideHelpUI()
    {
        helpUIPlaceHolder.SetActive(false);
    }
    #endregion

    #region Private Methods
    void ShowCurrentSlide()
    {
        if (lastVisitedIndex != currentIndex)
        {
            helpSlides[lastVisitedIndex].SetActive(false);
        }
        helpSlides[currentIndex].SetActive(true);
    }

    void HelpCloseBtnClk()
    {
        helpSlides[currentIndex].SetActive(false);
        HideHelpUI();
    }
    #endregion
}
