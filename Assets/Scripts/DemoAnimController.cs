using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DemoAnimController : MonoBehaviour
{
    // Start is called before the first frame update
    public Animator NurseAnim;
    private int _stepNum=0;
    public AudioClip[] StepSound ;
    public AudioSource AS;

    public void NextStep()
    {

        if (_stepNum == 0)
        {
            NurseAnim.SetTrigger("step0");
            AS.clip = StepSound[0];
            AS.Play();
        }
        else if (_stepNum == 1)
        {
            NurseAnim.SetTrigger("step1");
            AS.clip = StepSound[1];
            AS.Play();
        }
        else if (_stepNum == 2)
        {
            NurseAnim.SetTrigger("idle");
            AS.Stop();
        }

        if (_stepNum == 2)
            _stepNum = 0;
        else
            _stepNum++;
    }

    public void LoadHome()
    {
        Application.LoadLevel("UIScene");
    }

}
