﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MiddleScene : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {

        // SceneManager.LoadScene("AR Scene");
        StartCoroutine(WaitAndPrint());
    }


    private IEnumerator WaitAndPrint()
    {
     
            yield return new WaitForSeconds(10);
        SceneManager.LoadScene("AR Scene");
       
        
    }
  
}
