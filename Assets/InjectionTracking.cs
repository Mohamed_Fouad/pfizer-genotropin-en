﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Net;
using UnityEngine;
using UnityEngine.UI;

public class InjectionTracking : MonoBehaviour
{


    private string selectedPart = "";

    [SerializeField] private Text injectionStatusTxt;
    [SerializeField] private GameObject injectionStatusObj;

    private void Start()
    {
       // PlayerPrefs.DeleteAll();
        selectedPart = "";
        string testStr = "";
        testStr = GetNetTime().ToString();
      //  Debug.Log("TestStr1 "+testStr);

        string tempTimeStr = DateTime.Now.ToString();//"ddd, dd MMM yyyy HH:mm:ss 'GMT'");//"yyyy/MM/dd HH:mm:ss");//GetNetTimeString();
       // Debug.Log("selectedTime " + tempTimeStr);

        string testTime =  GetNetTimeString();
      //  Debug.Log("TestStr2 " + testTime);
      DateTime  testTime2 = ConvertStringToTime(testTime);
      //  Debug.Log("TestStr3 " + ""+testTime2);
        // DifferenceInDays();
       
    }

    public void CheckDays(string bodyPart)
    {

        string lastDate = PlayerPrefs.GetString(bodyPart, "firstTime");
        int daysNum = -1;
        selectedPart = bodyPart;

        if (!lastDate.Equals("firstTime"))
        {
            DateTime startDate = ConvertStringToTime(lastDate);
            DateTime currentDate = GetNetTime();

            daysNum = DifferenceInDays(currentDate, startDate);
           // Debug.Log("DifferenceInDays " + daysNum);

        }
        else
            daysNum = -1; // -1 indicates clear part (firsttime)

        string injectionStatus = "";

        if(daysNum > -1 && daysNum <6)
        {
            switch (daysNum)
            {
                case 0:
                    injectionStatus = "Today";
                    break;
                case 1:
                    injectionStatus = "Yesterday";
                    break;
                default:
                    injectionStatus = daysNum+" days ago";
                    break;
            }

            //showUI and Assign passed days values to text
            injectionStatusObj.SetActive(true);
            injectionStatusTxt.text = injectionStatus;
        }
        else
        {
            injectionStatusObj.SetActive(false);
            //Clear
        }
       

    }

    public void SelectBodyPart()
    {
        if (!selectedPart.Equals(""))
         {
            string tempTimeStr = GetNetTimeString();
          //  Debug.Log("selectedTime " + tempTimeStr);
            PlayerPrefs.SetString(selectedPart, tempTimeStr);
            PlayerPrefs.Save();

            PfizerAnalyicManager.Instance.FirePfAnalysis("Selected Injection Site" + selectedPart);
        }
        else
         {

            // No part selected 
         }
    }
 
    public static DateTime GetNetTime()
    {
        return DateTime.Now;
    }

    public static string GetNetTimeString()
    {
        return DateTime.Now.ToString("dd MMM yyyy HH:mm:ss");
    }

    public static DateTime ConvertStringToTime(string Time)
    {
        return DateTime.ParseExact(Time,
                                  "dd MMM yyyy HH:mm:ss",
                                  CultureInfo.InvariantCulture.DateTimeFormat,
                                  DateTimeStyles.AssumeUniversal);
    }

    public int DifferenceInDays (DateTime current, DateTime start)
    {
        TimeSpan difference = current - start;
      //  Debug.Log("Days :"+ difference.Days);
        return difference.Days;
    }

}
