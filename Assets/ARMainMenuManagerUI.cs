﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ARMainMenuManagerUI : MonoBehaviour
{

    #region Singleton
    private static ARMainMenuManagerUI _instance;
    public static ARMainMenuManagerUI Instance { get { return _instance; } }
    #endregion


    [Header("UI Assignment")]
    [SerializeField] Button homeBtn;
    [SerializeField] Button pfizerHomeBtn;
    [SerializeField] Button settingsBtn;
    [SerializeField] Button helpBtn;
    [SerializeField] Button injectionAreasBtn;
    [SerializeField] Button storageBtn;
    [SerializeField] Button bibliographyBtn;


    void Awake()
    {
        #region Singleton
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
        #endregion
    }

    // Start is called before the first frame update
    void Start()
    {
        homeBtn.onClick.AddListener(HomeBtnClk);
      
        settingsBtn.onClick.AddListener(MainMenuUIManager.Instance.SettingsBtnClk);
        helpBtn.onClick.AddListener(MainMenuUIManager.Instance.HelpBtnClk);
        injectionAreasBtn.onClick.AddListener(MainMenuUIManager.Instance.InjectionAreasBtnClk);
        storageBtn.onClick.AddListener(MainMenuUIManager.Instance.StorageBtnClk);
        bibliographyBtn.onClick.AddListener(MainMenuUIManager.Instance.BibliographyBtnClk);
       // settingsBtn.onClick.AddListener(MainMenuUIManager.Instance.SettingsBtnClk);
       // settingsBtn.onClick.AddListener(MainMenuUIManager.Instance.SettingsBtnClk);
    }

    private void HomeBtnClk()
    {
        MainMenuUIManager.Instance.ShowMainMenuUI();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
