﻿using UnityEngine;
using System.Runtime.InteropServices;
using UnityEngine.SceneManagement;
using Zappar;

public class Link : MonoBehaviour
{

	bool isSafari = false;
	bool isPDFOpened = false;
	bool isPWA = false;

	int fillHeightOrigin = 100;




	private void Start()
    {
		//CheckBrowser();

#if !UNITY_EDITOR && UNITY_WEBGL

fillHeightOrigin = getHeight();
CloseWindowJS();
#endif
	}
	public void OpenLinkJSPlugin(string pdfType)
	{

		//Application.OpenURL(URL);
		//if (ZapparCamera.Instance.m_cameraHasStarted)
		//	isPDFOpened = true;

		//if (isSafari && ZapparCamera.Instance.m_cameraHasStarted && isPDFOpened && isPWA)
		//	SceneManager.LoadScene("AR Scene");

		//fillHeightOrigin = getHeight();



#if !UNITY_EDITOR
		
if(pdfType == "somavert")
        {
		openPDFSomavert();
        }
		else if(pdfType == "terms")
        {
		openPDFTerms();
        }
	
#endif
	}

	void CheckBrowser()
	{
#if !UNITY_EDITOR && UNITY_WEBGL
            string browserVersion = GetBrowserVersion ();
      if (browserVersion.Contains("safari") || browserVersion.Contains("Safari"))
			isSafari = true;
		else
			isSafari =  false;

	 if (browserVersion.Contains("PWA"))
	  isPWA = true;
#endif
	}

	void CheckIfSafari(string browserVersionStr)
	{

		

		
	}

	public void OpenLinkURL(string URL)
	{
		Application.OpenURL(URL);
		if(ZapparCamera.Instance.m_cameraHasStarted)
			isPDFOpened = true;

		if (isSafari && ZapparCamera.Instance.m_cameraHasStarted && isPDFOpened && isPWA)
			SceneManager.LoadScene("AR Scene");

	}

	public void CloseWindowJS()
    {

		//Z.Initialize();

#if !UNITY_EDITOR

closeWindow();
        if (isSafari && ZapparCamera.Instance.m_cameraHasStarted && isPDFOpened)// && !isPWA)
        {
       // SceneManager.LoadScene("AR Scene");
        	//	closeWindow();
        	}
        		//if(fillHeightOrigin<1000)
        		//fillWindowRefresh(fillHeightOrigin);
        		//else
        		//	fillWindowRefresh(1000);
#endif
	}

#if !UNITY_EDITOR && UNITY_WEBGL
	[DllImport("__Internal")]
	private static extern void openWindow(string url);

	[DllImport("__Internal")]
	private static extern void closeWindow();

		[DllImport("__Internal")]
	private static extern int getHeight();

	[DllImport("__Internal")]
	private static extern void openPDFSomavert();

	[DllImport("__Internal")]
	private static extern void openPDFTerms();

		[DllImport("__Internal")]
	private static extern void fillWindowRefresh(int height);
#endif

#if !UNITY_EDITOR && UNITY_WEBGL
         [System.Runtime.InteropServices.DllImport("__Internal")] private static extern string GetBrowserVersion();
#endif


}